#Example 6 - Functions

#Functions are defined using the def statement and indented as usual by 4 spaces

#Ex6.1
print("Ex6.1")

def pounds(kg): #defining a function with a single argument (kg)
    lb=kg/.454
    print(lb)
    return lb  #return specifies the output of the function when called.



metric =10 #set this to the value in kg you want to convert
imperial=pounds(metric) #This calls the function with the argument (metric kg)
print ("there are "+str(imperial)+" pounds in "+ str(metric)+"kg")



#Ex6.2 #nothing returned and nothing as an argument
print ("\nEx6.2")
#Procedures are a special type of function which do not return a result
#However, the definition must still include () parentheses
#arguments are also optional

def writelines():
    print ("Engineers should like getting their hands dirty...")
    return()

writelines()#when calling the function the parentheses must be present
writelines()


#Ex6.3 # Multiple arguments, multiple returns
print ("\n\nEx6.3")
def string_analyse(string1,string2):
    length1=len(string1)
    length2=len(string2)
    print (str(length1)+" characters in "+string1)
    print (str(length2)+" characters in "+string2)
    return(length1, length2)


l1,l2=string_analyse("Engineers who avoid soldering will never be complete", "People who play policy instruments are the most misunderstood band members") #count characters in the two strings 

print l1
print l2                                                                                                                                     #Edit the arguments and rerun


