#Example 7 - Scoping of variables

#Variables exist at various levels depending on whether they are contained in functions, scripts or imported modules


#Example 7.1
#Variables introduced inside a function die when the function completes:
print ("\nEx7.1")

def dead_variable():
    doomedstring="I will not exist outside of this function"
    return

deadvariable() #run function dead_variable

try:                                             #Note the use of the try: except pair as an error handler... more on this later
    print doomedstring
except NameError:
    print ("You have a dead variable!")
    print ("Despite the variable 'doomedstring' being defined in the function 'dead_variable' it cannot be used outside of the function")



#Example 7.2
#Global variables
print("\nEx7.2")
#Don't be tempted t believe that globally declared variables exist system wide... they do not.
#Global declares variables to be active scoped for a single .py file
#As Mark Lutz puts it... global = active for my whole module

def cantkillthisvar():
    global immortal_var                         #declare immortal_var as scoped for this entire .py module must be inside def
    immortal_var="The War on Errorism"
    return
cantkillthisvar()           #call the funtion otherwise the variable will not be assigned!
print immortal_var          #and behold - the wonderment of a variable which persists outside of the function definition!



#Example 7.3 - Module variables
print ("\nEx7.3")
import Dummy_Module # reads in Dummy_Module.py
print ("PT100 Slope = "+str(Dummy_Module.PT100_slope))
#nano Dummy_Module.py to see what is happening here.
#we can also set variables which reside in the imported module
Dummy_Module.PT100_name="Brian"
print(Dummy_Module.PT100_name)


#Example 7.4
# Wider reaching variables
# There will be times when you want to set a state flag of marker to trigger another event.
# For example - a high water level might set a flag, which is then picked up by a script which activates a pump...
# Module imports and reloads could achieve this, however sometimes creating flag files can provide resilience to rebooting etc.
print("\nEx7.4")
#Using files to store variables which are needed to trigger action in other scripts or processes 
#I tend to use this method for determining that the clock has been synced with NTP server
#You could use this method to trigger a relay, send a text message etc etc.

import os, time
myfile=open("Dummy_flag",'r')
flagstring=myfile.read()        #Check out the contents of Dummy_flag change the contents using shell echo "REBOOT" > Dummy_flag for this script to follow through on the linked action.
myfile.close()

print (flagstring)
if str(flagstring) =="REBOOT":
    #action here:
    print "SYSTEM REBOOT REQUESTED - using flag state"
    time.sleep(2)      #sleep is a great way of giving your processor time to carry out other jobs. In this case it just delays the reboot so we can see the script working.
    os.system("sudo reboot")

    
#set up dummyflag for next time:
myfile =open("Dummy_flag",'w')
myfile.write("DONTREBOOT")
#myfile.write("REBOOT")
myfile.close()

