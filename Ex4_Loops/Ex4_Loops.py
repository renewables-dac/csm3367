###

#Example 4.1 Loop
#10 x repetition
print ("\nEx4.1\n")
a=1
while a< 11:         #note colon and indentation. Use 4 spaces to indent rather than tabs for consistency
    #anything at this level of indentation is included in the while loop
    print("Line "+str(a))
    a=a+1


#Example 4.2 
#While 1: with break and nested if
print ("\n\nEx4.2\n")
b=1
while 1:
    print ("While 1: line "+str(b)) #indented by 4 spaces as before
    b=b+1
    if b>5:
        print ("Break out") # indented by additional 4 spaces
        break #break terminates script


############################# Logical tests #########################
#
#  a < b           True if a less than b
#  a > b           True if a greater than b
#  a != b          True if a does not equal b
#  a == b          True if a equals b             ************ DOUBLE EQUALS SYMBOL! ***********
#  a <= b          True if a less than or equal to b
#  a >= b          True if a greater than or equal to b
#
########################################


#Example 4.3 and or
#Combinational or sequential logic.
print("\nEx4.3")

a=1
b=1
while 1:
    print ("\na = "+str(a))
    print ("b = "+str(b))
    if a >=100 and b >= 150:     #break out when both conditions are satisfied (try this using the or operator instead
        print("We have satisfied the condition...")
        break
    a=a+10 
    b=b+13 #increment counters
  









#################BONUS ######################


#Example 4.4
#not operator also available for inverting output of the truth test
print("\nEx4.4")
print ("while not a==b:")
a=10
b=1
while not a==b:
    print ("a is still not equal to b... a="+str(a)+" b="+str(b)) 
    a=a-1
print ("a=b    broken out of not loop")
