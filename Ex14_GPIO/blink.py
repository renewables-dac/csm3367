# Example of setting up GPIO pin as an output
# This is the equivalent of blink for the Arduino!

import os
import RPi.GPIO as GPIO	#import GPIO module
import time
GPIO.cleanup()
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)      #BCM uses Broadcom pin numbering
GPIO.setup(24,GPIO.OUT)     #define pin 24 as output pin


while 1:
    GPIO.output(24,GPIO.HIGH)
    print ("GPIO is HIGH")
    time.sleep(4)
    os.system("clear")		#clears the terminal window


    GPIO.output(24,GPIO.LOW)
    print("GPIO is LOW")
    time.sleep(4)	
    os.system("clear")		#Press CTRL+C to break out of the loop
