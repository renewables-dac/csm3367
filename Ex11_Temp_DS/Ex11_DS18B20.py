# Produces averaged timestamped temperature file for two temeprature probes on single wire bus
#Uses a configuration file to assign the probe hardware ID numbers
#Also demonstrates string manipulation to extract a number based on counting lines and searching for matching string fragments (Useful for RSS feed crunching!)
  
       
import time, os, Probe_Names             #Setup the Probe_Names.py Module to assign your probe hardware names DO THIS FIRST!

# ********************* DEFINE ALL VARIABLES

starttime=time #Record current time


#Activate temperature probe 
os.system('sudo modprobe w1-gpio')          #In the shell, when connecting the probes for the first time,
os.system('sudo modprobe w1-therm')         #You can type these commands, and look in the base directory
base_dir = '/sys/bus/w1/devices/'           #To determine the hardware addresses of the temperature probes
					    #TIP! Connect one at a time, so you know can name your probes.

#Probe path assigned from unique device name
device_path_T1 = (base_dir + Probe_Names.T1_ID + "/w1_slave")# LINKED TO THE CONFIG FILE
device_path_T2 = (base_dir + Probe_Names.T2_ID + "/w1_slave")# LINKED TO THE CONFIG FILE

print "\n\n"

#Define functions to obtain raw data strings from files in the w1 bus subfolder:
def read_temp_raw_T1():                           #The Raspberry Pi creates and maintains files containing the output from the DS18B20 probes
    f = open(device_path_T1, 'r')                 #All we need to do is read the files
    lines = f.readlines()
    f.close()
    return lines

def read_temp_T1():
        try:
                lines = read_temp_raw_T1()        #Once read, the files must be searched for the actual temperature value
                #print "\nT1 RAW DATA:\n"
                #print(lines)
                #yes denotes sensor functioning correctly          uncomment the prints to see what this script is doing if you like
                status_T1=(lines[0].strip()[-3:])
                #print ("STATUS T1 raw:\n"+status_T1)
                if str(status_T1) == "YES":                  #Annoyingly, temperature data is not always present, but if it is, the last three characters of the first line will be "YES"
                        time.sleep(0.2)
                        print("\nT1 Temperature present")
                        equals_pos=lines[1].find('t=')
                        
                        if equals_pos != -1:
                                temp_string=lines[1][equals_pos+2:]
                                print (temp_string)
                                temp_string=float(temp_string) #Convert string to a floating point number
                                temp_c1 = (round(temp_string/1000,2)) #convert the 4 or 5 digit number to DEG C and round to 2DP. (zeroes are not shown like on your calculator)
                                print temp_c1
                                return temp_c1
                else:
                        print ("\nT1 No Temperature data\n")   #when first line does not contain the "YES" string, abort. But a "YES" still means the device is connected and working...
        except:
                print ("\nNo device detected. Perhaps sensor 1 is unplugged?")   #If the try: fails to open the file, it means the sensor has been disconnected. 
                return "UNPLUGGED"                                             #We don't want our shole script to fail, so we return a warning string which should be picked up later

def read_temp_raw_T2():
    f = open(device_path_T2, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp_T2():
        try:
                lines = read_temp_raw_T2()
                status_T2=(lines[0].strip()[-3:])
                if str(status_T2) == "YES":
                        time.sleep(0.2)
                        print("\nT2 Temperature present")
                        equals_pos=lines[1].find('t=')
                        
                        if equals_pos != -1:
                                temp_string=lines[1][equals_pos+2:]
                                print (temp_string)
                                temp_string=float(temp_string)
                                temp_c2 = (round(temp_string/1000,2))
                                print(temp_c2)
                                return temp_c2
                else:
                        print ("\nT2 No Temperature data\n")
        except:
                print ("\nNo device detected. Perhaps sensor 2 is unplugged?")
                return "UNPLUGGED"



#main INSTRUCTIONS
#ESTABLISH DATA POINT TIMESTAMP
timestamp = (time.strftime("%Y-%m-%d %H:%M"))

#Obtain 6 sets of temperature data to allow for probe dormancy (the "NO" probe condition)
x=0
temp1sum=0
temp2sum=0

temp1_count=0
temp2_count=0

while x<4:                             #Four iterations seems to be a good number for this.
        print("\nRUN "+str((x+1)))
                
        temp1 = str(read_temp_T1())    #So now we are calling the functions to read the probe output files each time the loop runs
        temp2 = str(read_temp_T2())
        
        #Construct sum of T1,T2 and count for average
        if str(temp1)!="None":             # != mean not equal to, so if there is some data, do this: The "None" comes from the function Read_Temp_Tx() where the probe is in the "NO" state
                if str(temp1)=="UNPLUGGED": # A nested if to handle an unplugged sensor - no need to include in the mean value
                        print("Sensor 1 unplugged - skipping mean")     
                        temp1sum=666666#unplug flag   #create an unplug flag which will later signal to write an "UNPLUGGED" value to the XML string. If operating in the field, this would alert the operator to a problem.

                else:
                        temp1_count=temp1_count+1             #If the probe is working and returning data, then calculate the mean temperature
                        temp1sum=temp1sum+float(temp1)

        if str(temp2)!="None":
                if str(temp2)=="UNPLUGGED":
                        print("Sensor 2 unplugged - skipping mean")
                        temp2sum=666666#unplug flag
                else:
                        temp2_count=temp2_count+1
                        temp2sum=temp2sum+float(temp2)
 
        x=x+1                 #increment loop counter

#Calculate average temperatures with all possible sensor errors handled
if temp1sum!=0:
        if temp1sum!=666666:
                temp1=round(temp1sum/temp1_count,1)
else:
        temp1="None"
        if temp1sum==666666:
                temp1="UNPLUGGED"
        
if temp2sum!=0:
        if temp2sum!=666666:
                temp2=round(temp2sum/temp2_count,1)
else:
        temp2="None"
        if temp2sum==666666:
                temp2="UNPLUGGED"

#Construct output XML string
stringXML=("<Data_Point><Time_Stamp>"+str(timestamp) + "</Time_Stamp><Temp1>"+ str(temp1) +"</Temp1><Temp2>"+str(temp2)+"</Temp2></Data_Point>")
print(str(stringXML))                

print ("\nData set complete for "+starttime.strftime("%H:%M\n"))                       
                
#Append XML minute chunk to time stamped file
filename = "DS18B20_"+ starttime.strftime("TEMPERATURE_%Y-%m-%d.xml")
myfile=open(filename, "a")
myfile.write("\n"+stringXML)
myfile.close()

print ("      ***DATA SET WRITTEN TO FILE: "+filename+"  *** OK ***\n")

 


                        
                        

